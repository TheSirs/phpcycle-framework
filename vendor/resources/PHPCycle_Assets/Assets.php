<?php

class PHPCycle_Assets
{
    /**
     * SVG
     *
     * @param $file
     * @return bool|string
     */
    public static function svg($file)
    {
        $file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/svg/{$file}";

        if ( ! is_file($file) ) return false;

        return @ PHPCycle::read_file($file);
    }

    /**
     * PDF
     *
     * @param $file
     * @return bool|string
     */
    public static function pdf($file)
    {
        $file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/pdf/{$file}";

        if ( ! is_file($file) ) return false;

        return @ PHPCycle::read_file($file);
    }
    
    /**
     * Text
     *
     * @param $file
     * @param null $prepend
     * @return bool|string
     */
    public static function txt($file, $prepend = null)
    {
        $file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/txt/{$file}";

        if ( ! is_file($file) ) return false;

        return $prepend . @ PHPCycle::read_file($file);
    }

    /**
     * Font
     *
     * @param $file
     * @return bool|string
     */
    public static function font($file)
    {
        $file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/font/{$file}";

        if ( ! is_file($file) ) return false;

        return @ PHPCycle::read_file($file);
    }

    /**
     * Image
     *
     * @param $file
     * @return bool|string
     */
    public static function img($file)
    {
        $file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/img/{$file}";

        if ( ! is_file($file) ) return false;

        return @ PHPCycle::read_file($file);
    }

    /**
     * JavaScript
     *
     * @param $file
     * @param bool $minified
     * @param null $prepend
     * @return bool|string
     */
    public static function js($file, $minified = true, $prepend = null)
    {
        $file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/js/{$file}";

        if ( ! is_file($file) ) return false;

        return $minified === true
            ? $prepend . \JSMin::minify(@PHPCycle::read_file($file))
            : $prepend . @ PHPCycle::read_file($file);
    }

    /**
     * CSS (Cascading Stylesheet)
     * Compiles LessCSS is necessary
     *
     * @param $file
     * @param bool $minified
     * @param null $prepend
     * @return bool|string
     */
    public static function css($file, $minified = true, $prepend = null)
    {
        $pathinfo = pathinfo($file);

        $css_file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/css/" . @$pathinfo['dirname'] . '/' . $pathinfo['filename'] . '.css';
        $less_file = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/_less/" . @$pathinfo['dirname'] . '/' . $pathinfo['filename'] . '.less';

        if ( is_file($less_file) && filemtime($less_file) > filemtime($css_file) )
        {
            $css_dir = "../src/{$GLOBALS['phpcycle']['project']}/view/assets/css/" . $pathinfo['dirname'];

            if ( ! is_dir($css_dir) )
                mkdir($css_dir, 0777, true);

            file_put_contents( $css_file, (new lessc())->compile(PHPCycle::read_file($less_file)) );
        }

        /**
         * Fetch & return CSS file
         */
        $file = $css_file;

        if ( ! is_file($file) ) return false;

        return $minified === true
            ? $prepend . (new CSSmin())->run(PHPCycle::read_file($file))
            : $prepend . @ PHPCycle::read_file($file);
    }
}