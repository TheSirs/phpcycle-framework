<?php

class croncacheController
{
    public $files_cached = 0;
    public $users_cached = 0;

    public function __construct()
    {
        $_SESSION['logon'] = [
			'id' => 1        
        ];

        PHPCycle::load_project('admin', 'controller');

        $pagesModel = new pagesModel();
        $pagesModel->recache_pages();

        $indexController = new dashboardController();
        $indexController->echo = false;
        $indexController->pages(['pages']);

        $adminModel = new adminModel();
        $adminModel->getAllUsers();

        foreach ( $adminModel->list as $user )
        {
            Memc::set([
                [ "login-{$user['email']}", $user ]
            ]);

            $this->users_cached++;
        }

        @ $this->cache_files('../');

        echo 'Complete';
        echo "<br>Files cached: {$this->files_cached}";
        echo "<br>Users cached: {$this->users_cached}";
    }

    public function cache_files($path)
    {
        foreach ( glob("{$path}/*") as $i )
            if ( is_file($i) )
            {
                @ PHPCycle::read_file($i, true);

                $this->files_cached++;
            }

            else $this->cache_files($i);
    }
}