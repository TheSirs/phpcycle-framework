$(document).ready(function()
{
    /**
     * Menu underline
     */
    $('body > div.container > header').append('<div class="underline"></div>');

    var menuHover = function(e)
    {
        $('body > div.container > header > div.underline').css({
            left : $(e.target).position().left + 'px',
            top : $(e.target).position().top + 'px',
            width : $(e.target).width() + 'px',

            'border-color' : (
                $(e.target).hasClass('active') || $(e.target).parent().hasClass('active')
                    ? '#E38502' : '#b9b9b9'
            )
        });
    };

    menuHover({ target : $('body > div.container > header > ul.menu > li.active') });
    $('body > div.container > header > ul.menu > li').hover(menuHover);

    /**
     * Slide
     */
    (function slideAnimate()
    {
        $('ul.slider').css({
            'background-position-x' : (Math.round(Math.random() * 100)) + '%',
            'background-position-y' : (Math.round(Math.random() * 100)) + '%'
        })

        setTimeout(slideAnimate, 7000);
    })()

    /**
     * Email system
     */
    $('form.contact_form').submit(function() { return false });
    $('form.contact_form [type=submit]').click(function(e)
    {
        var el = this,
            data = {};

        for ( var i in j = $(this).parent().find('[name]') )
        {
            if ( ! ( i > -1 ) ) continue;
            if ( $(j[i]).attr('required') && ! $(j[i]).val() ) return false;

            data[ $(j[i]).attr('name') ] = $(j[i]).val();
        }
        
        $.ajax({
            url : window.location.protocol + '//' + window.location.host + '/api/sendmail',
            method : 'POST',
            data : data,
            success : function(data)
            {
                if ( data.result === true )
                    $(el).parent().addClass('sent')
            }
        });

        return false;
    })
});