<?php

class dashboardController
{
    public $echo = true;

    public function __construct()
    {
        $this->settings = new settingsModel();
    }

    public function index($para)
    {
        $page = @ $_SESSION['logon'] !== false
            ? 'index.html.twig' : 'login.html.twig';

        if ( $html = \Memc::get("adminPage-{$page}-" . $_SESSION['logon']['email']) ) exit($html);

        $html = \Twig::render('pages/' . $page, [
            'theme' => '1.0',
            'slug' => null,
            'settings' => $this->settings->get_settings_clean()
        ]);

        echo $html;
    }

    public function pages($para)
    {
        if ( $html = \Memc::get('adminPage-pages-' . @ $_SESSION['logon']['email']) ) exit($html);

        $pages = new \pagesModel();
        $pages->pageList();

        $html = \Twig::render('pages/pages.html.twig', [
            'theme' => '1.0',
            'slug' => $para[0],
            'pagelist' => $pages->list,
            'settings' => $this->settings->get_settings_clean()
        ]);

        if ( $this->echo === true )
            echo $html;
        else
            return $html;
    }

    public function media($para)
    {
        if ( $html = \Memc::get('adminPage-media-' . @ $_SESSION['logon']['email']) ) exit($html);
        
        $html = \Twig::render('pages/media.html.twig', [
            'theme' => '1.0',
            'slug' => $para[0],
            'settings' => $this->settings->get_settings_clean()
        ]);

        if ( $this->echo === true )
            echo $html;
        else
            return $html;
    }

    public function settings($para)
    {
        if ( $html = \Memc::get('adminPage-settings-' . @ $_SESSION['logon']['email']) ) exit($html);

        $html = \Twig::render('pages/settings.html.twig', [
            'theme' => '1.0',
            'slug' => $para[0],
            'settings' => $this->settings->get_settings_clean()
        ]);

        if ( $this->echo === true )
            echo $html;
        else
            return $html;
    }

    public function users($para)
    {
        if ( $html = \Memc::get('adminPage-users-' . @ $_SESSION['logon']['email']) ) exit($html);

        $html = \Twig::render('pages/users.html.twig', [
            'theme' => '1.0',
            'slug' => $para[0],
            'settings' => $this->settings->get_settings_clean()
        ]);

        if ( $this->echo === true )
            echo $html;
        else
            return $html;
    }
}