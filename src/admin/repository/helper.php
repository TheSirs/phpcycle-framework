<?php

class helper
{
    public static function dump($str, $die = false)
    {
        echo '<pre>';
        print_r($str);
        if ( $die ) exit;
        echo '</pre>';
    }

    /**
     * Checks bool & returns with string
     *
     * @param $con
     * @return string
     */
    public static function con($con)
    {
        return $con ? 'true' : 'false';
    }
}