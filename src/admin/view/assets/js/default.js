var error = function(txt)
{
    var el = $.parseHTML('<div class="alert alert-danger alert-dismissable" role="alert" style="display: none"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
        + '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '
        + txt
        + '</div>');

    $('#error_reporting, .alertbox').append(el);

    $(el).slideDown();
};

var success = function(txt)
{
    var el = $.parseHTML('<div class="alert alert-success alert-dismissable" role="alert" style="display: none"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
        + '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '
        + txt
        + '</div>');

    $('#error_reporting, .alertbox').append(el);

    $(el).slideDown();
};