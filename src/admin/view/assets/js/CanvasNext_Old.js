/**
 *   Top-level CanvasNext API
 *   http://canvasnext.com
 *
 *   Copyright (c) 2015 Chester Abrahams
 *
 *   Permission is hereby granted, free of charge, to any person
 *   obtaining a copy of this software and associated documentation
 *   files (the "Software"), to deal in the Software without
 *   restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the
 *   Software is furnished to do so, subject to the following
 *   conditions:
 *
 *   The above copyright notice and this permission notice shall be
 *   included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *   OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * @param opt
 * @constructor
 */
var CN = function (opt)
{
    this.init(opt);

    /**
     * Execute constructor-functions when
     * the app needs to be initialised
     */
    typeof this.OPT.construct == 'function' &&
    this.OPT.construct(),
        delete this.OPT.construct;

    /**
     * Initialise CanvasNext API
     */
    this.init_API();

    /**
     * Start generating frames by calling the running callback
     */
    this.callback(this);
};

/**
 * Initializer for Canvas element
 * Creates necessary objects
 *
 * @param opt
 */
CN.prototype.init = function (opt)
{
    var CN = this;

    /**
     * Choose between prefixes for requesting animation frames
     * @type {Function}
     */
    window.requestAnimationFrame = window.requestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.msRequestAnimationFrame;

    /**
     * Abbreviation for OPTIONS
     */
    this.OPT = opt;

    /**
     * Default options
     */
    /**
     * Used for storing numerous of variables & materials
     *
     * @type {{Task: Array, Obj:Array, Layers: Array, Img: {}, Audio: {}}}
     */
    this.STORE =
    {
        /**
         * Array of scheduled functions for every callback
         *
         * function (){}
         *
         * @type {object}
         */
        Task : [],

        /**
         * Array for linking objects linearly
         *
         * (* = optional properties)
         *     layer: (int),
         *     x: (int), y: (int),
         *     * position: 'absolute' || 'relative'
         *     * width: (int), *height: (int)
         *
         *     * bg_color: (String color)
         *     * image: (Img reference)
         *     * crop [(int X), (int Y),(int Width), int(Height)]
         *
         *     * rotate: (int)
         *     * clear (bool)
         *
         *     * fillStyle: (string)
         *     * text: [ text (string), font (string), fillStyle (string) ]
         *
         *     * audio (Audio reference)
         *  }
         *
         * @type {object}
         */
        Obj : [],

        /**
         * Layers array representing subsets for the frame buffer
         *
         *  // Individual layers
         *  {
             *      modified: (bool),
             *      canvas: (Canvas),
             *      ctx: (Ctx),
             *      obj: [ // Layer-objects
             *             obj: (Obj reference)
             *           ]
             *  }
         */
        Layers : [],

        /**
         * Array containing image objects
         * Two-dimensional object
         * * version_name "default" will always be inherited
         *
         *   *  {
             *      img_name: {
             *                  version_name (e.g daylight): img
             *               }
             *  }
         */
        Img : {},

        /**
         * Array containing audio objects
         */
        Audio : {}
    };

    /**
     * Refer to own client
     */
    this.STORE.CN = CN;

    /**
     * Custom observation, for multiple browser support
     *
     * @param obj
     * @param fcn
     * @param recursion
     */
    this.OPT.observer = function (obj, fcn, recursion)
    {
        /**
         * Chrome, Opera
         */
        if ( Object.observe )
        {
            if ( obj instanceof Object )
                Object.observe ( obj, function ( changes )
                {
                    fcn({
                        object : obj,
                        name : changes[0]['name'],
                        old_value : changes[0]['oldValue'],
                        new_value : changes[0]['newValue']
                    })
                } );

            for ( i = 0, len = obj.length ; i < len ; i++ )
                recursion === true && obj[i] instanceof Object &&
                CN.OPT.observer ( obj[i], fcn, recursion )
        }

        /**
         * FireFox
         */
        else if ( Object.watch )
        {
            for ( var i = 0, keys = Object.keys(obj), len = keys.length ; i < len ; i++ )
            obj.watch( keys[i], function ( name, old_value, new_value )
            {
                fcn({
                    object : obj,
                    name : name,
                    old_value : old_value,
                    new_value : new_value
                });

                return new_value
            } );

            recursion === true && obj[keys[i]] instanceof Object &&
            CN.OPT.observer ( obj, fcn, recursion );
        }
    }

    /**
     * Setting up context / game raster
     * Refer the raster to this.OPT.ctx
     *
     * @type {CanvasRenderingContext2D}
     */
    this.OPT.ctx = this.OPT.canvas.getContext('2d');

    /**
     * Used for calculating framerate per second
     * @count represents the actual framerate
     *
     * @type {{frameEnum: number, frameTime: number, count: number}}
     */
    this.OPT.frameCount =
    {
        frameEnum : 0,
        frameTime : 0,
        count : 0
    };

    /**
     *  Track the mouse position
     *  [ X, Y ]
     *
     *  or
     *
     *  @obj mouse.{ x, y }
     */
    CN.mouse_position = [0, 0];
    CN.mouse = new Object();
    this.OPT.canvas.onmousemove = function(e)
    {
        CN.mouse_position = [ e.pageX - CN.OPT.canvas.offsetLeft, e.pageY - CN.OPT.canvas.offsetTop ];

        CN.mouse.x = CN.mouse_position[0];
        CN.mouse.y = CN.mouse_position[1];
    };

    /**
     * Set visual processor of raster
     *
     * [
     *   @int, // indicates from which layer it should be drawn
     *   function(ctx) // your function
     * ]
     *
     * @type {Object}
     */
    this.visual_process = new Object();

    /**
     * Array consisting functions which take place in drawing the final frame
     * @type {Array}
     */
    this.raster_process = [];

    /**
     * Decides whether the final frame needs to be reconstructed
     * @type {boolean}
     */
    this.STORE.reconstruct_raster = false;

    /**
     * Initialised the camera system
     */
    this.OPT.camera =
    {
        x : 0,
        y : 0,
        z : 1,
        rotate : 0,
        width : this.OPT.canvas.width,
        height : this.OPT.canvas.height
    };

    // Set up camera observer. Reconstruct raster if when it's modified
    this.STORE.zoom_w =
        this.STORE.zoom_h = 0;

    this.OPT.observer( this.OPT.camera, function ()
    {
        CN.STORE.zoom_h = Math.abs( CN.OPT.camera.width / 2 - CN.OPT.camera.width / 2 * CN.OPT.camera.z );
        CN.STORE.zoom_w = Math.abs( CN.OPT.camera.height / 2 - CN.OPT.camera.height / 2 * CN.OPT.camera.z );

        if ( CN.OPT.camera.z > 1 )
            CN.STORE.zoom_w *= -1,
                CN.STORE.zoom_h *= -1;

        CN.STORE.reconstruct_raster = true;
    }, true );
    /**
     * -Default options
     */
};

/**
 * Decides which method to use for callback interval
 * Either requestAnimationFrame or recursive timeout
 *
 * @param CN
 */
CN.prototype.callback = function (CN)
{
    CN.controller();

    if ( CN.OPT.fps === 0 ) return;

    if ( window.requestAnimationFrame && ! CN.OPT.fps )
        window.requestAnimationFrame(function(){CN.callback(CN)});
    else
        setTimeout(function () { CN.callback(CN) }, 1000 / CN.OPT.fps);
}

CN.prototype.controller = function ()
{
    /**
     * First count the frames to extract a framerate
     */
    this.OPT.frameCount.frameEnum++;

    if ( this.OPT.frameCount.frameTime + 1000 < new Date().getTime() )
        this.OPT.frameCount.count = this.OPT.frameCount.frameEnum,
            this.OPT.frameCount.frameEnum = 0,
            this.OPT.frameCount.frameTime = new Date().getTime();

    /**
     * Create user-friendly alias of framecount
     */
    this.fpsCount = this.OPT.frameCount.count;

    /**
     * Run scheduled tasks
     */
    this.controller.tasks(this);

    /**
     * Draw the frame
     */
    this.raster();
}

CN.prototype.controller.tasks = function (CN)
{
    for ( var i in CN.STORE.Task )
        'function' == typeof CN.STORE.Task[i] &&
        CN.STORE.Task[i]()
};

/**
 * Draw into a CTX using an object identical to that
 * of the draw object
 *
 * @param ctx
 * @param obj
 * @param clear (boolean, say whether the ctx needs to be cleared or not)
 * @param no_pointer (boolean, true if the coordinates should be x0 y0)
 */
CN.prototype.drawCall = function (ctx, obj, clear, no_pointer)
{
    ctx.save();

    /**
     * If @param clear is specified and if property clear !== false, clear the CTX
     */
    clear && obj['clear'] !== false &&
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    /**
     * Perform rotation
     */
    if ( typeof obj['rotate'] != 'undefined' )
    {
        ctx.translate( obj['x'] + obj['width'] / 2, obj['y'] + obj['height'] / 2 );

        ctx.rotate( obj['rotate'] * Math.PI / 180 );

        ctx.translate( -(obj['x'] + obj['width'] / 2), -(obj['y'] + obj['height'] / 2) );
    }

    /**
     * X and Y axises are 0 by default
     */
    if ( no_pointer !== true )
        var x = obj['x'] || 0,
            y = obj['y'] || 0;
    else
        var x = y = 0;

    /**
     * Render text
     * Set optional fillStyle element
     */
    if ( typeof obj['text'] != 'undefined' )
        ctx.fillStyle = obj['text'][2],
            ctx.font = obj['text'][1],
            ctx.fillTexdt( obj['text'][0], obj['x'], obj['y'] );

    /**
     * Set fillStyle
     */
    if ( typeof obj['fillStyle'] != 'undefined' )
        ctx.fillStyle = obj['fillStyle'];

    /**
     * Draw a rectangle
     */
    if ( typeof obj['bg_color'] != 'undefined' )
        ctx.fillStyle = obj['bg_color'],
            ctx.fillRect(x, y, obj['width'], obj['height']);

    /**
     * Draw an image
     */
    if ( typeof obj['image'] != 'undefined' )
        if ( typeof obj['crop'] != 'undefined' )
            ctx.drawImage(
                ( obj['image'] instanceof HTMLElement
                    ? obj['image']
                    : obj['image']['default'] ),            // Image reference
                obj['crop'][0],                             // sx
                obj['crop'][1],                             // sy
                obj['crop'][2],                             // swidth
                obj['crop'][3],                             // sheight
                x,                                          // x
                y,                                          // y
                obj['crop'][2],                             // width
                obj['crop'][3]                              // height
            );

        else if ( obj['width'] != undefined && obj['height'] != undefined )
            ctx.drawImage(
                ( obj['image'] instanceof HTMLElement
                    ? obj['image']
                    : obj['image']['default'] ),            // Image reference
                x,                                          // sx
                y,                                          // sy
                obj['width'],                               // swidth
                obj['height']                               // sheight
            );

        else
            ctx.drawImage(
                ( obj['image'] instanceof HTMLElement
                    ? obj['image']
                    : obj['image']['default'] ),            // Image reference
                x,                                          // sx
                y                                           // sy
            );

    /**
     * Run a function for e.g low-level rendering
     */
    typeof obj['run'] == 'function' &&
    obj['run'](ctx);

    ctx.restore();
}

/**
 * The process of drawing a frame
 *
 * Quick overview:
 *      (Traverse individual layers)
 *          -> (Traverse individual layer-objects)
 *                  -> (Draw layer-object(s) in STORE canvas if necessary)
 *          -> (Reconstruct the layer if necessary)
 *      -> (Reconstruct the raster if necessary)
 */
CN.prototype.raster = function ()
{
    var layers = this.STORE.Layers,
        camera = this.OPT.camera;

    /**
     * Only draw something if bool reconstruct_raster is true
     */
    if ( this.STORE.reconstruct_raster === true  )

    /* Traverse individual layers */
        for ( var i_layer in layers )
        {
            /* Traverse layer-objects
             * To reconstruct the layer-object
             * and individual layer if necessary */
            if ( layers[i_layer].modified === false ) continue;

            /**
             * Specify whether you should NOT redraw the entire frame
             * Default false
             */
            layers[i_layer].clear !== false &&
            this.clearRect ( layers[i_layer].ctx );

            for ( var i_obj in layers[i_layer]['obj'] )
            {
                var obj = layers[i_layer]['obj'][i_obj];

                /**
                 * Mandatory: visible !== false
                 */
                if ( obj.visible === false ) continue;

                /**
                 * Only proceed if the object fits within the
                 * camera's viewport (for performance reasons)
                 */
                if ( layers[i_layer].optimize === true && ! (
                        ( obj.x >= camera.x && obj.x <= camera.width + camera.x )
                        &&  ( obj.y >= camera.y && obj.y <= camera.height + camera.y )
                    ) ) continue;

                /**
                 * Adjust the layer's dimensions if the object
                 * fits outside the current viewport
                 */
                if ( obj.x + obj.width > layers[i_layer].canvas.width )
                    layers[i_layer].canvas.width = obj.x + obj.width;

                if ( obj.y + obj.height > layers[i_layer].canvas.height )
                    layers[i_layer].canvas.height = obj.y + obj.height;

                /**
                 * Draw layer-object onto individual layer
                 */
                this.drawCall ( layers[i_layer].ctx, obj );

                this.STORE.reconstruct_raster = true;
            }

            /**
             * Perform the so-called "visual processing"
             * Simply a method presenting with the layer's context in the parameter
             */
            typeof layers[i_layer]['process'] == 'function' &&
            layers[i_layer]['process'] ( layers[i_layer].ctx );

            layers[i_layer].modified = false;
        }

    /**
     * Finally draw the final frame
     */
    if ( this.STORE.reconstruct_raster === true || 1 )
    {
        this.clearRect ( this.OPT.ctx );

        for ( var i_layer in layers )
        {
            /**
             * Draw layer relative to camera's coordinate, if the position is specified
             * Apply width X height zoom normalisation if the position is relative
             */
            if ( layers[i_layer].position == 'relative' )
                var x = camera.x - camera.x * 2 + this.STORE.zoom_w,
                    y = camera.y - camera.y * 2 + this.STORE.zoom_h,
                    width = layers[i_layer].canvas.width * camera.z,
                    height = layers[i_layer].canvas.height * camera.z,
                    rotate = camera.rotate;

            else
                var x = y = rotate = 0,
                    width = layers[i_layer].canvas.width,
                    height = layers[i_layer].canvas.height;

            this.drawCall ( this.OPT.ctx, {
                image : layers[i_layer].canvas,
                x : x,
                y : y,
                width : width,
                height : height,
                rotate : rotate
            }, false );

            /**
             * Run the raster virtual processor
             */
            for ( var i in this.raster_process )
                this.raster_process[i][0] == i_layer &&
                this.raster_process[i][1](this.OPT.ctx)
        }

        this.STORE.reconstruct_raster = false;
    }

    /**
     * Perform the so-called "visual processing"
     * Simply a method presenting with the raster's context in the parameter
     */
    typeof this.visual_process == 'function' &&
    this.visual_process ( this.OPT.ctx );
}

/**
 * Adds methods for outside applications to use
 */
CN.prototype.init_API = function ()
{
    /**
     * Create user-friendly aliases
     */
    this.ctx = this.OPT.ctx;
    this.canvas = this.OPT.canvas;

    this.list = this.STORE.Obj;
    this.obj = this.STORE.Obj;

    this.layer = this.STORE.Layers;
    this.layers = this.STORE.Layers;
    this.tasks = this.STORE.Task;

    this.images = this.STORE.Img;
    this.img = this.STORE.Img;

    this.audio = this.STORE.Audio;
    this.camera = this.OPT.camera;

    /**
     * User-friendly manage FPS limit
     */
    Object.defineProperty(this, 'fps', {
        get : function () { return this.OPT.fps },
        set : function ( value ) { this.OPT.fps = value }
    });

    /**
     * Create user-friendly viewport manager
     * CN.width, CN.height
     *
     * @type {number}
     */
        // Width
    Object.defineProperty(this, 'width', {
        get : function () { return this.STORE._width },
        set : function ( value ) {
            this.OPT.canvas.width = value;
            this.STORE._width = value;

            /**
             * Change dimension of every layer
             * & request to redraw every object
             */
            for ( var i in this.STORE.Layers )
                this.STORE.Layers[i].canvas.width = value,
                    this.STORE.Layers[i].modified = true;

            this.STORE.reconstruct_raster = true;
        }
    });

    // Height
    Object.defineProperty(this, 'height', {
        get : function () { return this.OPT._height },
        set : function ( value ) {
            this.OPT.canvas.height = value;
            this.OPT._height = value;

            /**
             * Change dimension of every layer
             * & request to redraw every object
             */
            for ( var i in this.STORE.Layers )
                this.STORE.Layers[i].canvas.height = value,
                    this.STORE.Layers[i].modified = true;

            this.STORE.reconstruct_raster = true;
        }
    });

    this.width = this.OPT.canvas.width;
    this.height = this.OPT.canvas.height;

    /**
     * Add an object to the store
     * & link it in the layers array
     *
     * @type number       // will just splice the index
     * @param only_layer (if you only want the link to be removed from the layer)
     */
    this.add_obj = function (obj, only_layer)
    {
        var list = this.STORE.Obj,
            layers = this.STORE.Layers,
            opt = this.OPT,
            CN = this;

        /**
         * The layer is 0 by default
         */
        obj.layer = obj.layer || 0;

        /**
         * X and Y axises are 0 by default
         */
        if ( obj.x == undefined ) obj.x = 0;
        if ( obj.y == undefined ) obj.y = 0;

        /**
         * Download image if the image property is a string (URL) (It won't add it to the list though)
         */
        if ( typeof obj.image == 'string' || typeof obj.img == 'string' )
            obj.image = this.add_img(obj.image || obj.img, undefined, obj);

        /**
         * Add to list
         */
        only_layer !== true &&
        list.push(obj);

        /**
         * Observe the list's object's properties
         * Set bool property "modified" of the list & containing
         * layer to true
         *
         * Move object's later if necessary by:
         * 1. remove from current layer
         * 2. add to new layer
         */
        if( typeof obj.observer == 'undefined' )
            obj.observer = this.OPT.observer( list[list.length - 1], function (changes)
            {
                if ( changes['name'] != 'modified' )
                    if ( changes['name'] == 'layer' )
                    {
                        changes['object'].layer_old = changes['old_value'];
                        CN.remove_obj( changes['object'], true );
                        CN.add_obj( changes['object'], true );

                        for ( var i = CN.STORE.Layers.length - 1 ; i >= 0 ; --i )
                            if ( CN.STORE.Layers[i].obj.length == 0 )
                                CN.STORE.Layers.splice(i);
                            else
                                break;
                    }

                    else if ( typeof changes['object'].layer != 'undefined' )
                        layers[changes['object'].layer].modified = true;

                /**
                 * Start drawing
                 */
                CN.STORE.reconstruct_raster = true;
            }, true );

        /**
         * If layer does not exist, recursively extend array
         */
        if ( typeof layers[obj.layer] == 'undefined' )
            for ( var i = 0 ; i <= obj.layer ; i++ )
                if ( typeof layers[i] == 'undefined' )
                {
                    var canvas = document.createElement('canvas'),
                        ctx = canvas.getContext('2d');

                    canvas.width = opt.canvas.width;
                    canvas.height = opt.canvas.height;

                    layers.push({
                        modified : true,
                        canvas : canvas,
                        ctx : ctx,
                        obj : [],

                        clear : true,
                        optimize : false,
                        position : 'relative',
                        process : function() {}     // Visual processing method
                    });

                    /**
                     * Set observer for this layer
                     * Mainly to trigger modification when the process function changes
                     */
                    CN.OPT.observer( layers[layers.length - 1], function (changes)
                    {
                        if ( typeof changes['object'] != 'undefined' && changes['name'] != 'modified' )
                            changes['object'].modified = true;

                        CN.STORE.reconstruct_raster = true
                    } );
                }

        /**
         * Finally add the object to the layer
         * Also set layer's bool "modified" to true
         */
        layers[obj.layer].obj.push( obj );
        layers[obj.layer].modified = true;

        /**
         * Start drawing
         */
        this.STORE.reconstruct_raster = true;

        return list[list.length - 1];
    }

    /**
     * Removes an object from the layers array
     * as well from the assigned layer
     *
     * @param obj
     * @param only_layer (if you only want the link to be removed from the layer)
     * @type number     // will just splice the index
     * @type obj        // will iteratively search for object-containing index
     */
    this.remove_obj = function (obj, only_layer)
    {
        var list = this.STORE.Obj,
            layers = this.STORE.Layers;

        /**
         * Refer the object
         */
        if ( typeof obj == 'number' )
            var index = obj,
                obj = list[obj];
        else
            for ( var i in list )
                if ( list[i] == obj )
                {
                    var index = i;
                    obj = list[i];

                    break;
                }

        /**
         * Layer scalar
         * @type {Array|*}
         */
        var layer = obj.layer || obj.layer_old,
            layer_subject = layers[layer]['obj'];

        /**
         * Remove object from layer
         */
        for ( var i in layer_subject )
            if ( obj == layer_subject[i] )
                layer_subject.splice(i, 1),
                    layers[layer].modified = true;

        /**
         * Remove object from list
         * If only_layer === true
         */
        only_layer !== true &&
        list.splice(index, 1);

        /**
         * Remove layer_old property once defined
         */
        typeof obj.layer_old == 'undefined' &&
        delete obj.layer_old;

        /**
         * Start drawing
         */
        this.STORE.reconstruct_raster = true;
    }

    /** Empties a layer from all of its game objects **/
    this.empty_layer = function(i)
    {
        this.STORE.Layers[i].obj = [];
    }

    /**
     * Adds image to the images store
     * Choose whether to download the image by string depending on
     * the alias being defined
     */
    this.add_img = function (data, alias, subject)
    {
        var CN = this,
            list = this.STORE.Img,
            img = document.createElement('img');

        if ( typeof data == 'string' )
            img.src = data;
        else
            img = data;

        /**
         * Redraw the layer's object when the image is loaded
         */
        if ( typeof subject != 'undefined' )
        {
            var load_img = function (img, subject, CN) {
                setTimeout(function() {
                    if ( ! img.complete )
                        load_img(img, subject, CN);
                    else
                        CN.STORE.Layers[subject.layer].modified = true;
                }, 1)
            };

            load_img(img, subject, CN);
        }

        return typeof alias != 'undefined'
            ? list[alias] = img
            : img;
    }

    /**
     * Removes object from the images store
     */
    this.remove_img = function (alias)
    {
        delete this.STORE.Img[alias];
    }

    /**
     * Adds audio to the audio store
     * Choose whether to download the image by string depending on
     * the alias being defined
     */
    this.add_audio = function (data, alias)
    {
        var list = this.STORE.Audio,
            audio = document.createElement('audio');

        if ( typeof data == 'string' )
            audio.src = data;
        else
            audio = data;

        return typeof alias != 'undefined'
            ? list[alias] = audio
            : audio;
    }

    /**
     * Removes object from the audio store
     */
    this.remove_audio = function (alias)
    {
        var list = this.STORE.Audio;

        list[alias].pause();
        list[alias].currentTime = 0;

        delete list[alias];
    }

    /**
     * Clear a single context
     */
    this.clearRect = function (ctx)
    {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }
}