var CanvasNext = function(opt)
{
    this.initialize(opt);
    this.callback();
};

CanvasNext.prototype.initialize = function(opt)
{
    var CN = this;

    /**
     * Prepare STORE objects
     */
    this.STORE = opt;
    {
        /**
         * Declare CTX
         */
        this.STORE.CTX = this.STORE.canvas.getContext('2d');

        /**
         * 1-dimensional array of objects. Newly added objects will be appended to this list
         * @type {Array}
         *
         * *=optional
         * {
         *  *modified : int(1|2) // 1 = Only coordinates & rotation. 2 = Entire object needs to be redrawn
         *
         *  layer : int,
         *  *position : 'relative'|'absolute',
         *
         *  *rotate : int,
         *  *zoom : float
         *
         *  *x : int,
         *  *y : int,
         *  *width : int,
         *  *height : int,
         *
         *  *bg_color : string
         *  *img|image : <img>|string(url)
         *  *crop : [ int(x), int(y), int(width), int(height) ],
         *
         *  *audio : <audio>|string(url)
         *
         *  *text : [ string(text), string(font), fillStyle(string) ]
         * }
         */
        this.STORE.OBJ = [];

        /**
         * 1-dimensional array of layer objects. They are the subset buffers for the raster
         *
         * {
         *  modified : bool,
         *  canvas : <canvas>,
         *  obj : [] // 1-dimensional list of objects associated to this layer
         *  position : 'relative'|'absolute' // 'relative' draws layers relative to the camera's position while 'absolute' draws layers statically
         * }
         */
        this.STORE.LAYERS = [];
    }

    /**
     * Decide which callback system to use. Int(timeout)|AnimationFrame
     * @type {number|*}
     */
    this.STORE.FPS = ( this.STORE.prefixFPS = function(value)
    {
        if ( value === 0 )
        {
            this.frameCount.frameEnum =
                this.frameCount.count = 0;

            return 0
        }

        return Math.abs(value) || (window.requestAnimationFrame = window.requestAnimationFrame
                || window.mozRequestAnimationFrame
                || window.webkitRequestAnimationFrame
                || window.msRequestAnimationFrame)
    } )(opt.fps);
    /**
     * Used for calculating frames per second
     *
     * @count represents the second's frame rate
     * @type {{frameEnum: number, frameTime: number, count: number}}
     */
    this.STORE.frameCount =
    {
        frameEnum : 0,
        frameTime : 0,
        count : 0
    };

    /**
     * Camera system
     */
    this.STORE.camera =
    {
        x : 0,
        y : 0,
        z : 1,
        rotate : 0
    };

    /**
     * Find prefixes for Object.observe
     *
     * @function parameter { object, name, old_value, new_value }
     * @type {*|Function}
     */
    this.STORE.observer =
        // Chromium
        Object.observe && function(obj, fcn, recursion)
        {
            if ( obj instanceof Object )
                var obsv = Object.observe ( obj, function ( changes )
                {
                    fcn({
                        object : obj,
                        name : changes[0]['name'],
                        old_value : changes[0]['oldValue'],
                        new_value : changes[0]['newValue'] || changes[0]['object'][changes[0].name]
                    })
                } );

            if ( recursion === true  )
                for ( i = 0, len = obj.length ; i < len ; i++ )
                    obj[i] instanceof Object && CN.STORE.observer ( obj[i], fcn, true );

            return obsv
        }

            // Firefox
        || Object.watch && function(obj, fcn, recursion)
        {
            for ( var i = 0, keys = Object.keys(obj), len = keys.length ; i < len ; i++ )
                  var obsv = obj.watch( keys[i], function ( name, old_value, new_value )
                  {
                      fcn({
                          object : obj,
                          name : name,
                          old_value : old_value,
                          new_value : new_value
                      });

                      return new_value
                  } );

            recursion === true && obj[keys[i]] instanceof Object &&
            CN.STORE.observer ( obj, fcn, recursion );

            return obsv
        };

    /**
     * Allow for Object- Canvas dimension control
     */
        // Width
    Object.defineProperty(this, 'width', {
        get : function () { return this.STORE.canvas.width },
        set : function ( value ) { this.STORE.canvas.width = value }
    });

    // Height
    Object.defineProperty(this, 'height', {
        get : function () { return this.STORE.canvas.height },
        set : function ( value ) { this.STORE.canvas.height = value }
    });

    /**
     * Allow Object- FPS control
     *
     * @get return STORE.fpsRate
     */
    Object.defineProperty(this, 'fps', {
        get : function () { return this.STORE.frameCount.count },
        set : function ( value ) {
            clearTimeout(this.STORE.callback_timer);
            this.STORE.callback_timer = undefined;
            this.STORE.FPS = this.STORE.prefixFPS(value);
            this.callback()
        }
    });

    /**
     * Add drawing canvas element. Primarely used for layer & object drawing
     */
    var draw_canvas = document.createElement('canvas');
    this.STORE.draw_canvas =
    {
        canvas : draw_canvas,
        ctx : draw_canvas.getContext('2d')
    };
};

CanvasNext.prototype.callback = function()
{
    /**
     * Enumerate the frames count & declare second's frame rate
     */
        // Enumerate the frame count with every callback
        this.STORE.FPS !== 0 && this.STORE.frameCount.frameEnum++;
    
        // Declare the second's frame rate & clear the enumeration by the threshold
        if ( this.STORE.frameCount.frameTime + 1000 < new Date().getTime() )
            this.STORE.frameCount.count = this.STORE.frameCount.frameEnum,
                this.STORE.frameCount.frameEnum = 0,
                this.STORE.frameCount.frameTime = new Date().getTime();

    this.rasterize();

    /**
     * Don't trigger a new callback if the threshold FPS is at 0
     */
    if ( this.STORE.FPS === 0 ) return;

    if ( this.STORE.FPS === requestAnimationFrame )
        this.STORE.callback_timer = requestAnimationFrame(this.callback.bind(this));
    else
        this.STORE.callback_timer = setTimeout(this.callback.bind(this), 1000 / this.STORE.FPS)
}

/**
 * Draw into a CTX using an object
 *
 * @param ctx
 * @param obj
 * @param opt (optional)
 *      {
 *          x : int,
 *          y : int
 *      }
 */
CanvasNext.prototype.draw = function (ctx, obj, opt)
{
    opt = opt || {};

    ctx.save();

    /**
     * Clear rect if specified
     */
    if ( opt.clear === true )
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    var x = obj.x || opt.x || 0,
        y = obj.y || opt.y || 0;

    /**
     * Perform rotation
     */
    if ( typeof obj['rotate'] != 'undefined' )
    {
        ctx.translate( obj['x'] + obj['width'] / 2, obj['y'] + obj['height'] / 2 );

        ctx.rotate( obj['rotate'] * Math.PI / 180 );

        ctx.translate( -(obj['x'] + obj['width'] / 2), -(obj['y'] + obj['height'] / 2) );
    }
    /**
     * Render text
     * Set optional fillStyle element
     */
    if ( typeof obj['text'] != 'undefined' )
        ctx.fillStyle = obj['text'][2],
        ctx.font = obj['text'][1],
        ctx.fillText( obj['text'][0], obj['x'], obj['y'] );

    /**
     * Set fillStyle
     */
    if ( typeof obj['fillStyle'] != 'undefined' )
        ctx.fillStyle = obj['fillStyle'];

    /**
     * Draw a rectangle
     */
    if ( typeof obj['bg_color'] != 'undefined' )
        ctx.fillStyle = obj['bg_color'],
            ctx.fillRect(x, y, obj['width'], obj['height']);

    /**
     * Draw an image
     */
    if ( typeof obj['image'] != 'undefined' && obj.image.complete )
        if ( typeof obj['crop'] != 'undefined' )
            ctx.drawImage(
                ( obj['image'] instanceof HTMLElement
                    ? obj['image']
                    : obj['image']['default'] ),            // Image reference
                obj['crop'][0],                             // sx
                obj['crop'][1],                             // sy
                obj['crop'][2],                             // swidth
                obj['crop'][3],                             // sheight
                x,                                          // x
                y,                                          // y
                obj['crop'][2],                             // width
                obj['crop'][3]                              // height
            );

        else if ( obj['width'] != undefined && obj['height'] != undefined )
            ctx.drawImage(
                ( obj['image'] instanceof HTMLElement
                    ? obj['image']
                    : obj['image']['default'] ),            // Image reference
                x,                                          // sx
                y,                                          // sy
                obj['width'],                               // swidth
                obj['height']                               // sheight
            );

        else
            ctx.drawImage(
                ( obj['image'] instanceof HTMLElement
                    ? obj['image']
                    : obj['image']['default'] ),            // Image reference
                x,                                          // sx
                y                                           // sy
            );

    /**
     * Run a function for e.g low-level rendering
     */
    typeof obj['run'] == 'function' &&
    obj['run'](ctx);

    ctx.restore();
}

CanvasNext.prototype.rasterize = function()
{
    // Todo: Explain!!!

    var layers = this.STORE.LAYERS,
        camera = this.STORE.camera;

    for ( var layer in layers )
    {
        var target_layer = layers[layer];

        if ( target_layer.modified !== true ) continue;

        target_layer.ctx.clearRect(0, 0, target_layer.canvas.width, target_layer.canvas.height);

        // Attempt to rescale layer's canvas' size to the furthest object
        for ( var obj in layers[layer].obj )
        {
            var target_obj = layers[layer].obj[obj];

            if ( target_obj.x + target_obj.width > target_layer.canvas.width )
                target_layer.canvas.width = target_obj.x + target_obj.width;

            if ( target_obj.y + target_obj.height > target_layer.canvas.height )
                target_layer.canvas.height = target_obj.y + target_obj.height;
        }

        for ( var obj in layers[layer].obj )
        {
            var target_obj = layers[layer].obj[obj];

            if ( target_obj.buffer !== true && ! (
                    ( target_obj.x + target_obj.width >= camera.x && target_obj.x <= camera.x + this.STORE.canvas.width )
                    && ( target_obj.y + target_obj.height >= camera.y && target_obj.y <= camera.y + this.STORE.canvas.height )
                ) ) continue;

            target_obj.modified = false;

            this.draw(target_layer.ctx, target_obj);
        }

        target_layer.modified = false;
    }

    /**
     * Finally draw the layers onto the raster
     */
    this.STORE.CTX.clearRect(0, 0, this.STORE.canvas.width, this.STORE.canvas.height);

    for ( var i in layers )
        this.draw(this.STORE.CTX, {
            image : layers[i].canvas,
            
            x : layers[i].position == 'relative' && -camera.x || 0,
            y : layers[i].position == 'relative' && -camera.y || 0,
            rotate : camera.rotate,
            
            width : layers[i].canvas.width * camera.z,
            height : layers[i].canvas.height * camera.z
        })
}

CanvasNext.prototype.add_obj = function(prop)
{
    var CN = this,
        list = this.STORE.OBJ,
        layers = this.STORE.LAYERS;

    var obj = {};

    /**
     * Connects observer to added object
     * Also re-distributes object to new layer if layer is modified
     */
    if ( typeof obj.observer == 'undefined' )
        obj.observer = this.STORE.observer( obj, function(changes)
        {
            console.log(changes.name)

            if ( changes.name == 'modified' ) return;

            // Set object's modified status
            obj.modified = true;

            // If a new image is being added
            if ( obj.modified == 'image' )
            {
                if ( typeof changes['new_value'] == 'string' )
                {
                    var img = document.createElement('img');
                        img.src = changes['new_value'];

                    return obj.image = img
                }

                console.log('passed');

                obj.image.onload = function()
                {
                    console.log(this);
                }
            }

            // If the change's name is *not* layer, meaning we don't need to swap,
            // -simply set modified to true
            if ( changes.name != 'layer' )
                layers[obj.layer-1].modified = true;

            else
            {
                var obj_ref = changes['object'];

                CN.remove_obj( obj_ref, changes.old_value );

                obj_ref.layer = changes['new_value'];

                CN.add_obj( obj_ref )
            }
        }, true );

    for ( var i in prop )obj[i] = prop[i];

    /**
     * Set default values for common properties
     */
    obj.x = obj.x || 0;
    obj.y = obj.y || 0;
    obj.modified = true;

    /**
     * If the layer doesn't exist, iteratively create the necessary layers
     */
    if ( typeof layers[obj.layer-1] == 'undefined' )
        for ( var i = layers.length ; i < obj.layer ; i++ )
            this.add_layer();

    layers[obj.layer-1].obj.push(obj);
    list.push(obj);

    /**
     * The layer's position can be initialized through the object
     */
    layers[obj.layer-1].position =
        layers[obj.layer-1].position || obj.position || 'relative';

    return obj
}

CanvasNext.prototype.add_layer = function(obj)
{
    obj = obj || {};

    obj.modified = true;
    obj.obj = obj.obj || [];

    obj.canvas = document.createElement('canvas');
    obj.ctx = obj.canvas.getContext('2d');

    this.STORE.LAYERS.push(obj);

    /**
     * Connects observer to added layer
     */
    if ( typeof obj.observer == 'undefined' )
        obj.observer = this.STORE.observer( obj, function(changes)
        {
            if ( changes.name == 'modified' ) return;

            obj.modified = true
        } );
}

CanvasNext.prototype.remove_obj = function(obj, target_layer)
{
    var list = this.STORE.OBJ,
        layer = this.STORE.LAYERS[(target_layer || obj.layer)-1];

    // Remove this object from the associated layer
    for ( var i in layer.obj )
        if ( layer.obj[i] == obj )
        {
            layer.obj.splice(i, 1);
            break
        }

    // Remove this object from the objects list
    for ( var i in list )
        if ( list[i] == obj )
        {
            list.splice(i, 1);
            break
        }
}