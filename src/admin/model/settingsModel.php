<?php

class settingsModel
{
    private $MyCRUD;

    public $data;
    public $data_clean = [];

    public $head =
        [
            '#' => [
                'visible' => false
            ],
            'Key' => [
                'raw' => '{content}'
            ],
            'Value' => [
                'element' => 'input',
                'type' => 'text',
                'placeholder' => 'Value',
                'class' => 'form-control',

                'column' => 'value'
            ],
            'Head' => [
                'visible' => false
            ],
            'Group' => [
                'visible' => false
            ],
            'last_updated' => [
                'visible' => false
            ]
        ];

    public function __construct($schema = true)
    {
        $this->MyCRUD = new \app\MyCRUD($GLOBALS['mysqli']['interpersona'], [
            'tables' => 'settings',
            'columns' => [
                'settings.key.VARCHAR(500)',
                'settings.value.LONGTEXT',
                'settings.head.VARCHAR(500) DEFAULT \'{}\'',
                'settings.group.VARCHAR(500)',
                'settings.last_updated.DATETIME DEFAULT CURRENT_TIMESTAMP'
            ]
        ]);
    }

    public function get_setting($key)
    {
        if ( ! $this->data ) $this->get_settings_clean();

        return @ $this->data_clean[$key];
    }

    public function get_settings($filter = '', $order = '')
    {
        if ( !empty($filter) ) $filter = "AND {$filter}";
        if ( !empty($order) ) $filter = ", {$order}";

        return $this->data = $this->MyCRUD->pull([
            'filter' => $filter,
            'order' => "settings.group ASC, settings.key ASC {$order}"
        ], true);
    }
    
    public function get_settings_clean()
    {
        $data_clean = $this->data = $this->MyCRUD->pull([
            'order' => "settings.group ASC, settings.settings_id DESC"
        ], true);
        
        foreach ( $data_clean as $i => $j )
            $this->data_clean[$j['key']] = $j['value'];

        return $this->data_clean;
    }

    public function update_settings($id, $column, $value)
    {
        $this->MyCRUD->update($column, "settings.settings_id = '{$id}'", $value);
        $this->MyCRUD->debug(true);
        $this->MyCRUD->update('last_updated', "settings.settings_id = '{$id}'", 'NOW()');
    }
}