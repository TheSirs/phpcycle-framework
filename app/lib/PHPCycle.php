<?php

class PHPCycle
{
    /**
     * Either open file from Memcache or the filesystem
     *
     * @param $path
     * @return array|string
     */
    public static function read_file($path, $replace = false)
    {
        // Use regular method in case MEMCACHE is unavailable
        if ( ! MEMCACHE ) return file_get_contents($path);

        $path = realpath($path);

        $content = @ Memc::get($path);

        if ( $content !== false && $replace === false )
            return $content;

        else
        {
            $content = file_get_contents($path);

            @ Memc::set([
                [ $path, $content, 60 * 60, MEMCACHE_COMPRESSED ]
            ]);

            return $content;
        }
    }

    /**
     * Either require files (once) from Memcache or the filesystem
     */
    public static function require_file($path, $replace = false)
    {
        // Use regular method in case MEMCACHE is unavailable
        if ( ! MEMCACHE ) return require_once $path;

        $path = realpath($path);

        // Find out if file was already required
        foreach ( @ (array) $GLOBALS['phpcycle']['required_files'] as $i )
            if ( $path == $i ) return;

        $content = @ Memc::get($path);

        if ( $content !== false && $replace === false )
        {
            $GLOBALS['phpcycle']['required_files'][] = $path;
             @ eval('?>' . $content);
        }

        else
        {
            $content = @ file_get_contents($path);

            @ Memc::set([
                [ $path, $content, MEMCACHE_COMPRESSED ]
            ]);

            $GLOBALS['phpcycle']['required_files'][] = $path;

            @ eval('?>' . $content);
        }
    }

    /**
     * Use GLOB using Memcache
     */
    public static function glob($pattern, $replace = false)
    {
        // Use regular method in case MEMCACHE is unavailable
        if ( ! MEMCACHE ) return glob($pattern);

        $content = @ Memc::get('glob_' . $pattern);

        if ( $content !== false && $replace === false )
            return $content;

        else
        {
            $content = glob($pattern);

            @ Memc::set([
                [ 'glob_' . $pattern, $content, MEMCACHE_COMPRESSED ]
            ]);

            return $content;
        }
    }

    /**
     * Loads PHP files from a project
     * @param $project
     */
    public static function load_project($project, $add_dirs = array())
    {
        \PHPCycle\router::load_project($project, $add_dirs);
    }
}