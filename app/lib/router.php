<?php

namespace PHPCycle;


class router
{
    /**
     * Recursively load route files from a given path
     * @param $path
     */
    public function load_route_contents($path)
    {
        foreach ( \PHPCycle::glob("{$path}/*") as $subject )
            if ( is_dir($subject) )
                $this->load_route_contents($subject);
            else
                $GLOBALS['phpcycle']['routeContents'][$subject] = simplexml_load_string( \PHPCycle::read_file($subject) );
    }

    /**
     * Iterate route contents from GLOBALS->phpcycle->routeContents
     * -until pattern match with current URI is made
     *
     * 1. Match possible host attribute
     * 2. Recursively iterate possible group matches
     * 3. Iterate & match routes
     */
    public function find_route($key, $xml, $uri = null, $param = array(), $attr = array())
    {
        // Postpone iteration if route is already set
        if ( ! is_null($GLOBALS['phpcycle']['route']) ) return;

        // Match possible host attribute or postpone iteration
        if ( $xml->attributes()->host && ! preg_match("|^{$xml->attributes()->host}|", $_SERVER['HTTP_HOST']) )
            return;

        /**
         * Iterate & match routes
         *
         * If match is made:
         *  1. Define project
         *  2. Set route_parameters
         *  3. Set route_attributes
         *  4. Refer route object
         */
        foreach ( $xml->route as $route )
            if ( is_null($GLOBALS['phpcycle']['route'])
                && preg_match("|^{$uri}{$route->attributes()->pattern}|", $_SERVER['REQUEST_URI'], $uri_match) )
            {
                preg_match('|../src/(.*)/routes|', $key, $project);

                $GLOBALS['phpcycle']['project'] = $project[1];
                $GLOBALS['phpcycle']['route_parameters'] = array_merge($param, array_splice($uri_match, 1));
                $GLOBALS['phpcycle']['route_attributes'] = array_merge($attr, current($route->attributes()));
                $GLOBALS['phpcycle']['route'] = $route;

                $GLOBALS['phpcycle']['controller'] = (string) @ $GLOBALS['phpcycle']['route_attributes']['controller'];
                $GLOBALS['phpcycle']['method'] = (string) @ $GLOBALS['phpcycle']['route_attributes']['method'];

                unset($GLOBALS['phpcycle']['routeContents']);

                return;
            }

        /**
         * Recursively iterate possible group matches if following conditions are true:
         *
         * 1. If either host is not present OR current host matches host attribute
         * 2. The pattern matches the pattern attribute
         */
        foreach ( $xml->group as $group )
            if ( is_null($GLOBALS['phpcycle']['route'])
                    && ( empty($group->attributes())
                        || preg_match("|^{$uri}{$group->attributes()->pattern}|", $_SERVER['REQUEST_URI'], $uri_match) ) )
                            $this->find_route($key,
                                $group,
                                @ $uri_match[0],
                                array_merge($param, @ (array) array_splice($uri_match, 1)),
                                array_merge($attr, @ (array) current($group->attributes())) );
        }

    /**
     * Recursively loads all of the models & repositories
     * @param $project
     */
    public static function load_project($project, $add_dirs = array())
    {
        foreach ( (array) $add_dirs as $dir )
            self::load_php_files("../src/{$project}/{$dir}");

        self::load_php_files("../src/{$project}/model");
        self::load_php_files("../src/{$project}/repository");
    }

    /**
     * Executes route extensions from associative array in GLOBALS
     *
     * @param:
     *  @route,
     *  @route_parameters,
     *  @route_attributes
     */
    public function execute_route_extensions()
    {
        foreach ( $GLOBALS['phpcycle']['route_attributes'] as $route => $value )
            foreach ( $GLOBALS['route_extensions'] as $tag => $function )
                if ( $tag == $route )
                    $function (
                        $GLOBALS['phpcycle']['route_parameters'],
                        $GLOBALS['phpcycle']['route_attributes']
                    );
    }

    /**
     * Load a specified controller class & method
     *
     * The parameters for both the constructor & method:
     *  route_parameters, route_attributes
     */
    public function load_controller()
    {
        $controllerClass = end(explode('/', $GLOBALS['phpcycle']['controller']));
        $file = "../src/{$GLOBALS['phpcycle']['project']}/controller/{$GLOBALS['phpcycle']['controller']}.php";

        if ( is_file($file) )
        {
            \PHPCycle::require_file($file);

            $controller = new $controllerClass(
                $GLOBALS['phpcycle']['route_parameters'],
                $GLOBALS['phpcycle']['route_attributes']
            );

            if ( ! empty($GLOBALS['phpcycle']['method']) )
                $controller->{$GLOBALS['phpcycle']['method']}(
                    $GLOBALS['phpcycle']['route_parameters'],
                    $GLOBALS['phpcycle']['route_attributes']
                );
        }
    }

    /**
     * Recursively requires PHP files from a directory
     * @param $dir
     */
    private static function load_php_files($dir)
    {
        foreach ( \PHPCycle::glob("{$dir}/*") as $subject )
            if ( is_dir($subject) )
                self::load_php_files($subject);
            else
                \PHPCycle::require_file($subject);
    }
}