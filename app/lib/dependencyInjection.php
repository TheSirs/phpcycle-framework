<?php

namespace PHPCycle;


class dependencyInjection
{
    /**
     * Require given dependency injections
     * Change current directory so that resources are easier to include
     */
    public static function injectDependencies()
    {
        $prev_dir = getcwd();
        chdir('../vendor/resources/');

        foreach ( $GLOBALS['dependencies'] as $file )
            \PHPCycle::require_file("../injectors/{$file}");

        chdir($prev_dir);
    }
}