<?php

/**
 * First off look for and instantiate Memcache(d)
 */
$GLOBALS = array_merge_recursive( $GLOBALS, @ (array) json_decode(file_get_contents('../ini/memcache.json')) );

if ( $GLOBALS['use_memcache'] !== false && extension_loaded('Memcached') )
    require_once 'lib/Memcache/MemcacheD.php';
else
    require_once 'lib/Memcache/Memcache.php';

/**
 * Validate whether Memcache works or not
 * Constant MEMCACHE should be used for verification
 */
if ( $GLOBALS['use_memcache'] !== false )
{
    @ Memc::set([ [$mem_key = time() . rand(1000000, 9999999), true] ]);

    if ( Memc::get($mem_key) === true )
        define('MEMCACHE', true);
    else
        define('MEMCACHE', false);

    @ Memc::delete($mem_key);
}

else define('MEMCACHE', false);

/**
 * Require all libraries
 */
foreach ( glob('lib/*.php') as $file ) require_once $file;

/**
 * 1. Instantiate iniLoader object
 * 2. Load & merge all of the app/PHPCycle ini files
 * 3. Load & merge all of the ROOT ini files
 */
$iniLoader = new PHPCycle\iniLoader();
    $iniLoader->load_files('ini');
    $iniLoader->merge_ini();
            $iniLoader->load_files('../ini');
            $iniLoader->merge_ini();

/**
 * Load specified ROOT dependencies
 */
\PHPCycle\dependencyInjection::injectDependencies();

/**
 * 1. Instantiate Router object
 * 2. Iterate projects & load route contents
 * 3. Iterate route contents until matching URI pattern is found
 */
$router = new PHPCycle\router();
    $router->load_route_contents('../src/*/routes');
        foreach ( $GLOBALS['phpcycle']['routeContents'] as $key => $xml ) $router->find_route($key, $xml);

/**
 * 1. Load & merge all of the PROJECT ini files
 * 2. Load project's models & repositories
 * 3. Load specified PROJECT dependencies
 * 4. Execute route attribute-specified extensions
 * 5. Call (optional) controller & method
 */

$iniLoader->load_files("../src/{$GLOBALS['phpcycle']['project']}/ini");
$iniLoader->merge_ini();
        $router->load_project($GLOBALS['phpcycle']['project']);
            \PHPCycle\dependencyInjection::injectDependencies();
                $router->execute_route_extensions();
                    $router->load_controller();